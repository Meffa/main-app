from flask import Flask, render_template, request, redirect, url_for
from werkzeug.utils import secure_filename
import pickle
import os

app = Flask(__name__)


@app.route('/')
def upload_file():
    print('''    Median house value for California districts predictor
    Input text file with one instance per line.
    Values should be integer or float and comma-separated. One instance should include:
    
        - Median income in block
        - Median house age in block
        - Average number of rooms
        - Average number of bedrooms
        - Block population
        - Average house occupancy
        - House block latitude
        - House block longitude
        
    ''')
    with open('model.pkl', 'rb') as f:
        model = pickle.load(f)
    inp = input('Input 8 features of numeric type divided with spaces:\n')
    inp = [[float(feat.strip()) for feat in inp.split()]]
    res = model.predict(inp)
    return f'Answer: {res}\n'


if __name__ == '__main__':
    app.run("0.0.0.0")
